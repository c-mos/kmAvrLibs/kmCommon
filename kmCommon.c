/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//**
* @brief CHANGEME: BRIEF DESCRIPTION.
* kmCommon.c
*
*  **Created on**: Jan 25, 2025 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include "kmCommon.h" 

#ifdef KM_GTEST

namespace kmCommon {

    // Unit tests for KMC_ARRAY_SIZE macro.
    TEST(kmCommonDefs_KMC_ARRAY_SIZE, it_Should_Calculate_Array_Size) {
        // Test for a simple integer array
        int arr1[5] = { 1, 2, 3, 4, 5 };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr1), 5);

        // Test for a character array
        char arr3[10] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr3), 10);

        // Test for a float array
        float arr4[4] = { 1.1f, 2.2f, 3.3f, 4.4f };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr4), 4);

        // Test for a multi-dimensional array
        int arr5[3][4] = { {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12} };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr5), 3);  // Number of rows

        // Test for a pointer (this should fail since KMC_ARRAY_SIZE is for arrays, not pointers)
        int* ptr = arr1;
        // Note: this will give incorrect result since `sizeof(ptr)` is the size of the pointer, not the array
        // This is for demonstration purposes, do not use in actual code.
        EXPECT_NE(KMC_ARRAY_SIZE(ptr), 5);  // Should not be 5, because ptr is just a pointer, not an array
    }

    // Tests for KMC_ARRAY_SIZE with different types of arrays.
    TEST(kmCommonDefs_KMC_ARRAY_SIZE, it_Should_Work_With_Different_Types) {
        // Test for an array of integers
        int intArr[10] = { 0 };
        EXPECT_EQ(KMC_ARRAY_SIZE(intArr), 10);

        // Test for an array of doubles
        double doubleArr[3] = { 1.1, 2.2, 3.3 };
        EXPECT_EQ(KMC_ARRAY_SIZE(doubleArr), 3);

        // Test for an array of characters
        char charArr[6] = { 'a', 'b', 'c', 'd', 'e', 'f' };
        EXPECT_EQ(KMC_ARRAY_SIZE(charArr), 6);
    }

    // Tests for the KMC_ARRAY_SIZE macro with edge cases.
    TEST(kmCommonDefs_KMC_ARRAY_SIZE, it_Should_Handle_Edge_Cases) {
        // Test for an array of size 1
        int arr1[1] = { 5 };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr1), 1);

        // Test for a large array
        int arr2[1000] = { 0 };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr2), 1000);

        // Test for a single element of type double
        double arr3[1] = { 0.0 };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr3), 1);
    }

    // Tests for KMC_ARRAY_SIZE with a complex structure.
    TEST(kmCommonDefs_KMC_ARRAY_SIZE, it_Should_Work_With_Structures) {
        struct MyStruct {
            int a;
            float b;
        };

        MyStruct arr4[4] = { {1, 1.1f}, {2, 2.2f}, {3, 3.3f}, {4, 4.4f} };
        EXPECT_EQ(KMC_ARRAY_SIZE(arr4), 4);
    }

    // Tests for KMC_ARRAY_SIZE with an array of pointers.
    TEST(kmCommonDefs_KMC_ARRAY_SIZE, it_Should_Work_With_Array_Of_Pointers) {
        int* arr[4] = { NULL, NULL, NULL, NULL };  // Array of 4 pointers to int
        EXPECT_EQ(KMC_ARRAY_SIZE(arr), 4);  // This works since arr is an array of 4 pointers
    }
    

    // Tests for KMC_MIN

    // Tests KMC_MIN with two positive integers
    TEST(kmCommonDefs_KMC_MIN, it_Should_ReturnSmaller_WhenBothPositive) {
        EXPECT_EQ(2, KMC_MIN(2, 5));
        EXPECT_EQ(1, KMC_MIN(1, 100));
    }

    // Tests KMC_MIN with mixed positive and negative values
    TEST(kmCommonDefs_KMC_MIN, it_Should_ReturnNegative_WhenMixedSigns) {
        EXPECT_EQ(-10, KMC_MIN(-10, 5));
        EXPECT_EQ(-50, KMC_MIN(0, -50));
    }

    // Tests KMC_MIN when both values are equal
    TEST(kmCommonDefs_KMC_MIN, it_Should_HandleEqualValues) {
        EXPECT_EQ(7, KMC_MIN(7, 7));
        EXPECT_EQ(-3, KMC_MIN(-3, -3));
    }

    // Tests KMC_MIN with floating-point values
    TEST(kmCommonDefs_KMC_MIN, it_Should_WorkWithFloatingPointNumbers) {
        EXPECT_FLOAT_EQ(1.5, KMC_MIN(1.5, 3.7));
        EXPECT_DOUBLE_EQ(-2.3, KMC_MIN(-2.3, 5.6));
    }

    // Tests KMC_MIN with character values
    TEST(kmCommonDefs_KMC_MIN, it_Should_WorkWithCharValues) {
        EXPECT_EQ('a', KMC_MIN('a', 'z'));
        EXPECT_EQ('A', KMC_MIN('Z', 'A'));
    }

    // Tests KMC_MIN with arithmetic expressions
    TEST(kmCommonDefs_KMC_MIN, it_Should_WorkWithExpressions) {
        EXPECT_EQ(5, KMC_MIN(4 + 1, 9 - 2));
        EXPECT_EQ(3, KMC_MIN(3 * 2, 7 - 4));
    }

    // Tests for KMC_MAX

    // Tests KMC_MAX with two positive integers
    TEST(kmCommonDefs_KMC_MAX, it_Should_ReturnLarger_WhenBothPositive) {
        EXPECT_EQ(5, KMC_MAX(2, 5));
        EXPECT_EQ(100, KMC_MAX(1, 100));
    }

    // Tests KMC_MAX with mixed positive and negative values
    TEST(kmCommonDefs_KMC_MAX, it_Should_ReturnPositive_WhenMixedSigns) {
        EXPECT_EQ(5, KMC_MAX(-10, 5));
        EXPECT_EQ(0, KMC_MAX(0, -50));
    }

    // Tests KMC_MAX when both values are equal
    TEST(kmCommonDefs_KMC_MAX, it_Should_HandleEqualValues) {
        EXPECT_EQ(7, KMC_MAX(7, 7));
        EXPECT_EQ(-3, KMC_MAX(-3, -3));
    }

    // Tests KMC_MAX with floating-point values
    TEST(kmCommonDefs_KMC_MAX, it_Should_WorkWithFloatingPointNumbers) {
        EXPECT_FLOAT_EQ(3.7f, KMC_MAX(1.5f, 3.7f));
        EXPECT_DOUBLE_EQ(5.6, KMC_MAX(-2.3, 5.6));
    }

    // Tests KMC_MAX with character values
    TEST(kmCommonDefs_KMC_MAX, it_Should_WorkWithCharValues) {
        EXPECT_EQ('z', KMC_MAX('a', 'z'));
        EXPECT_EQ('Z', KMC_MAX('Z', 'A'));
    }

    // Tests KMC_MAX with arithmetic expressions
    TEST(kmCommonDefs_KMC_MAX, it_Should_WorkWithExpressions) {
        EXPECT_EQ(7, KMC_MAX(4 + 1, 9 - 2));
        EXPECT_EQ(6, KMC_MAX(3 * 2, 7 - 4));
    }

    // Main function to run all tests
    int main(int argc, char** argv) {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    }


    // Tests for KMC_CLAMP macro.
    TEST(kmCommonDefs_KMC_CLAMP, it_should_return_clamped_value) {
        int result = KMC_SIGNED_ZERO;

        // Test case: x is greater than the high boundary
        result = KMC_CLAMP(100, 200, 300);
        EXPECT_EQ(result, 200);

        // Test case: x is less than the low boundary
        result = KMC_CLAMP(100, 50, 90);
        EXPECT_EQ(result, 90);

        // Test case: x is within the range
        result = KMC_CLAMP(150, 100, 200);
        EXPECT_EQ(result, 150);
    }

    // Tests for edge cases with the KMC_CLAMP macro.
    TEST(kmCommonDefs_KMC_CLAMP, it_should_handle_edge_cases) {
        // Test case: x is equal to the low boundary
        EXPECT_EQ(KMC_CLAMP(100, 100, 200), 100);
    
        // Test case: x is equal to the high boundary
        EXPECT_EQ(KMC_CLAMP(200, 100, 200), 200);

        // Test case: all values are the same
        EXPECT_EQ(KMC_CLAMP(100, 100, 100), 100);

        // Test case: negative numbers
        EXPECT_EQ(KMC_CLAMP(-50, -100, 0), -50);
        EXPECT_EQ(KMC_CLAMP(-150, -100, 0), -100);
    }

    // Tests floating-point values with the KMC_CLAMP macro.
    TEST(kmCommonDefs_KMC_CLAMP, it_should_work_with_floating_point) {
        // Floating point tests
        EXPECT_FLOAT_EQ(KMC_CLAMP(5.5, 0.0, 10.0), 5.5);
        EXPECT_FLOAT_EQ(KMC_CLAMP(-3.2, 0.0, 10.0), 0.0);
        EXPECT_FLOAT_EQ(KMC_CLAMP(15.8, 0.0, 10.0), 10.0);
    }

    // Tests different types (int, float, etc.) with KMC_CLAMP.
    TEST(kmCommonDefs_KMC_CLAMP, it_should_work_with_various_types) {
        // Integer test
        EXPECT_EQ(KMC_CLAMP(25, 10, 30), 25);

        // Float test
        EXPECT_DOUBLE_EQ(KMC_CLAMP(12.75, 10.0, 20.0), 12.75);

        // Character test (evaluates based on ASCII values)
        EXPECT_EQ(KMC_CLAMP('c', 'a', 'z'), 'c');
        EXPECT_EQ(KMC_CLAMP('z', 'a', 'm'), 'm');
    }

    // Tests the macro with complex expressions.
    TEST(kmCommonDefs_KMC_CLAMP, it_should_handle_complex_expressions) {
        int a = 5, b = 10, c = 15;
        EXPECT_EQ(KMC_CLAMP(a + b, 10, 20), 15);
        EXPECT_EQ(KMC_CLAMP(a * 3, 10, 20), 15);
    }

}

#endif /* KM_GTEST */