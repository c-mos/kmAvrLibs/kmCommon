/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @brief Useful macros for value manipulation and calculations. Constants and conversion factors for use in time and bit manipulation.
*
* This header file defines several macros for working with values, such as
* getting the string representation of a value, finding the minimum and maximum
* of two values, and clamping a value within a specified range. Also contains definitions
* of various constants and conversion factors that are commonly used for bit
* manipulation and time conversions.
* kmCommonDefs.h
*
*  **Created on**: Aug 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmCommon library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KM_COMMON_DEFS_H_
#define KM_COMMON_DEFS_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
* @def KMC_VALUE_TO_STRING(x)
* @brief Converts a value to its string representation.
*
* This macro converts the given value @p x to its string representation.
* For example, if you pass 42 as @p x, it will be converted to the string "42".
*/
#define KMC_VALUE_TO_STRING(x) #x

/**
* @def KMC_VALUE(x)
* @brief Macro to get the string representation of a value.
*
* This macro uses `VALUE_TO_STRING` to convert the value @p x to its string
* representation. For example, if you pass a variable named `myVar` with the
* value 42, `VALUE(myVar)` will yield the string "myVar=42".
*/
#define KMC_VALUE(x) KMC_VALUE_TO_STRING(x)

/**
* @def KMC_VAR_NAME_VALUE(var)
* @brief Macro to get a variable name and its value as a string.
*
* This macro takes a variable @p var and returns a string that includes
* the variable name followed by its value, separated by an equal sign (=).
* For example, if you pass `myVar` with the value 42, `VAR_NAME_VALUE(myVar)`
* will yield the string "myVar=42".
*/
#define KMC_VAR_NAME_VALUE(var) #var "="  KMC_VALUE(var)

/**
* @def KMC_MIN(A, B)
* @brief Macro to find the minimum of two values.
*
* This macro takes two values @p A and @p B and returns the smaller of the two.
* It is a type-safe way to find the minimum value between two variables.
*/
#ifndef _MSC_VER   // Check if compiling with MSVC
#define KMC_MIN(A,B)    ({ __typeof__(A) __a = (A); __typeof__(B) __b = (B); __a < __b ? __a : __b; })
#else /* _MSC_VER */
#define KMC_MIN(A, B) ((A) < (B) ? (A) : (B))
#endif /* _MSC_VER */

/**
* @def KMC_MAX(A, B)
* @brief Macro to find the maximum of two values.
*
* This macro takes two values @p A and @p B and returns the larger of the two.
* It is a type-safe way to find the maximum value between two variables.
*/
#ifndef _MSC_VER   // Check if compiling with MSVC
#define KMC_MAX(A,B)    ({ __typeof__(A) __a = (A); __typeof__(B) __b = (B); __a < __b ? __b : __a; })
#else /* _MSC_VER */
#define KMC_MAX(A, B)  ((A) > (B) ? (A) : (B))
#endif /* _MSC_VER */

/**
* @def KMC_CLAMP(x, low, high)
* @brief Macro to clamp a value within a specified range.
*
* This macro takes a value @p x, a lower bound @p low, and an upper bound @p high.
* It ensures that @p x is within the range [@p low, @p high] and returns it.
* If @p x is less than @p low, it returns @p low, and if @p x is greater than
* @p high, it returns @p high. Otherwise, it returns @p x itself.
*/
#ifndef _MSC_VER   // Check if compiling with MSVC
#define KMC_CLAMP(x, low, high) ({\
	__typeof__(x) __x = (x); \
	__typeof__(low) __low = (low);\
	__typeof__(high) __high = (high);\
	__x > __high ? __high : (__x < __low ? __low : __x);\
})
#else /* _MSC_VER */
#define KMC_CLAMP(x, low, high) ((x) > (high) ? (high) : ((x) < (low) ? (low) : (x)))
#endif /* _MSC_VER */

/**
 * @def KMC_ARRAY_SIZE(array)
 * @brief Retrieve the number of elements in a static array.
 *
 * This macro computes the number of elements in a given static array.
 * It is useful for determining the size of an array at compile time.
 * Note that this macro should not be used with pointers as it will not give
 * the correct result.
 *
 * @param a The array whose size is to be determined. The array must be
 *          a statically allocated array.
 *
 * @return The number of elements in the array.
 *
 * @note This macro relies on the `sizeof` operator and works only with
 *       statically allocated arrays. Using this macro with a pointer
 *       will result in incorrect output, typically 1.
 *
 * @code
 * #include <stdio.h>
 *
 * int main() {
 *     uint8_t my_array[] = {1, 2, 3, 4, 5};
 *     printf("Array size: %zu\n", ARRAY_SIZE(my_array));
 *     return 0;
 * }
 * @endcode
 * 
 * Outputs: Array size: 5
 */
#define KMC_ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

/**
 * @brief Convert a uint8_t value to a binary string.
 *
 * This macro takes a uint8_t value and converts it into a string representation
 * where each bit of the value is represented by either '0' or '1'.
 *
 * Example usage:
 * @code
 * uint8_t value = 0xAA;
 * const char *binaryString = UINT8_TO_BINARY_STRING(value);
 * printf("Binary representation: %s\n", binaryString);
 * @endcode
 *
 * @param byte The uint8_t value to be converted.
 * @return A string representing the binary value.
 */
#define KMC_UINT8_TO_BINARY_STRING(byte) \
    ( \
        ((byte) & 0x80 ? '1' : '0'), \
        ((byte) & 0x40 ? '1' : '0'), \
        ((byte) & 0x20 ? '1' : '0'), \
        ((byte) & 0x10 ? '1' : '0'), \
        ((byte) & 0x08 ? '1' : '0'), \
        ((byte) & 0x04 ? '1' : '0'), \
        ((byte) & 0x02 ? '1' : '0'), \
        ((byte) & 0x01 ? '1' : '0'), \
        '\0' \
    )

/**
 * @brief Convert a uint16_t value to a binary string.
 *
 * This macro takes a uint16_t value and converts it into a string representation
 * where each bit of the value is represented by either '0' or '1'.
 *
 * Example value, 0x55AA in binary is 0101010110101010
 * Example usage:
 * @code
 * uint16_t value = 0x55AA;
 * const char *binaryString = UINT16_TO_BINARY_STRING(value);
 * printf("Binary representation: %s\n", binaryString);
 * @endcode
 * 
 * @param byte The uint16_t value to be converted.
 * @return A string representing the binary value.
 */
#define KMC_UINT16_TO_BINARY_STRING(byte) \
    ( \
        ((byte) & 0x8000 ? '1' : '0'), \
        ((byte) & 0x4000 ? '1' : '0'), \
        ((byte) & 0x2000 ? '1' : '0'), \
        ((byte) & 0x1000 ? '1' : '0'), \
        ((byte) & 0x0800 ? '1' : '0'), \
        ((byte) & 0x0400 ? '1' : '0'), \
        ((byte) & 0x0200 ? '1' : '0'), \
        ((byte) & 0x0100 ? '1' : '0'), \
        ((byte) & 0x80 ? '1' : '0'), \
        ((byte) & 0x40 ? '1' : '0'), \
        ((byte) & 0x20 ? '1' : '0'), \
        ((byte) & 0x10 ? '1' : '0'), \
        ((byte) & 0x08 ? '1' : '0'), \
        ((byte) & 0x04 ? '1' : '0'), \
        ((byte) & 0x02 ? '1' : '0'), \
        ((byte) & 0x01 ? '1' : '0'), \
        '\0' \
    )


/**
* @def KMC_FALSE
* @brief Constant represengint the 'false' value compatible with MISRA standards
*/
#define KMC_FALSE (bool)false

/**
* @def KMC_TRUE
* @brief Constant represengint the 'true' value compatible with MISRA standards
*/
#define KMC_TRUE (bool)true

/**
* @def KMC_UNSIGNED_ZERO
* @brief Constant representing the unsigned value ZERO.
*/
#define KMC_SIGNED_ZERO 0x00

/**
* @def KMC_UNSIGNED_ZERO
* @brief Constant representing the unsigned value ZERO.
*/
#define KMC_UNSIGNED_ZERO 0x00u

/**
* @def KMC_INDEX_ZERO
* @brief Constant representing the initial index for loops.
*/
#define KMC_INDEX_ZERO KMC_UNSIGNED_ZERO

/**
 * @def KMC_UNSIGNED_ONE
 * @brief Constant representing the unsigned value one.
 */
#define KMC_UNSIGNED_ONE 0x01u

/**
 * @def KMC_SIGNED_ONE
 * @brief Constant representing the signed value one.
 */
#define KMC_SIGNED_ONE 0x01

/**
 * @def KMC_SIGNED_MINUS_ONE
 * @brief Constant representing the signed value minus one.
 */
#define KMC_SIGNED_MINUS_ONE -1

/**
 * @def KMC_INDEX_INCREASE
 * @brief Constant representing the unsigned value one used to increase index.
 */
#define KMC_INDEX_INCREASE 0x01u

/**
 * @def KMC_INDEX_INCREASE
 * @brief Constant representing the unsigned value one used to decrease index.
 */
#define KMC_INDEX_DECREASE 0x01u

/**
 * @def KMC_ALL_BITS_CLEARED
 * @brief Constant representing all bits cleared (zeroed).
 */
#define KMC_ALL_BITS_CLEARED 0x00u

/**
 * @def KMC_4_OUT_OF_8_BIT_MASK_LO
 * @brief 4 out of 8 bits mask for the low nibble (4 bits).
 */
#define KMC_4_OUT_OF_8_BIT_MASK_LO 0x0fu

/**
 * @def KMC_4_OUT_OF_8_BIT_MASK_HI
 * @brief 4 out of 8 bits mask for the high nibble (4 bits).
 */
#define KMC_4_OUT_OF_8_BIT_MASK_HI 0xf0u

/**
 * @def KMC_7_OUT_OF_16_BIT_MASK_LO
 * @brief 7 out of 16 bits mask for the low byte signed value (7 bits).
 */
#define KMC_7_OUT_OF_16_BIT_MASK_LO 0x7fu

/**
 * @def KMC_8_OUT_OF_16_BIT_MASK_LO
 * @brief 8 out of 16 bits mask for the low byte (8 bits).
 */
#define KMC_8_OUT_OF_16_BIT_MASK_LO 0xffu

/**
 * @def KMC_8_OUT_OF_16_BIT_MASK_HI
 * @brief 8 out of 16 bits mask for the high byte (8 bits).
 */
#define KMC_8_OUT_OF_16_BIT_MASK_HI 0xff00u

/**
 * @def KMC_16_OUT_OF_32_BIT_MASK_LO
 * @brief 16 out of 32 bits mask for the low byte (8 bits).
 */
#define KMC_16_OUT_OF_32_BIT_MASK_LO 0xffffu

/**
 * @def KMC_16_OUT_OF_32_BIT_MASK_HI
 * @brief 16 out of 32 bits mask for the high byte (8 bits).
 */
#define KMC_16_OUT_OF_32_BIT_MASK_HI 0xffff0000u

/**
 * @def KMC_7_BIT_SHIFT
 * @brief Shift value of 9 bits.
 */
#define KMC_7_BIT_SHIFT 0x07u

/**
 * @def KMC_8_BIT_SHIFT
 * @brief Shift value of 8 bits.
 */
#define KMC_8_BIT_SHIFT 0x08u

/**
 * @def KMC_9_BIT_SHIFT
 * @brief Shift value of 9 bits.
 */
#define KMC_9_BIT_SHIFT 0x09u

/**
 * @def KMC_4_BIT_SHIFT
 * @brief Shift value of 4 bits.
 */
#define KMC_4_BIT_SHIFT 0x04u

/**
 * @def KMC_1_BIT_SHIFT
 * @brief Shift value of 1 bit.
 */
#define KMC_1_BIT_SHIFT 0x01u

/**
* @def KMC_DIV_BY_2
* @brief Constant representing for 1bit right shift (division by 2).
*/
#define KMC_DIV_BY_2 0x01u

/**
* @def KMC_DIV_BY_4
* @brief Constant representing for 2bit right shift (division by 4).
*/
#define KMC_DIV_BY_4 0x02u

/**
* @def KMC_DIV_BY_8
* @brief Constant representing for 3bit right shift (division by 8).
*/
#define KMC_DIV_BY_8 0x03u

/**
* @def KMC_DIV_BY_16
* @brief Constant representing for 4bit right shift (division by 16).
*/
#define KMC_DIV_BY_16 0x04u

/**
* @def KMC_DIV_BY_32
* @brief Constant representing for 5bit right shift (division by 32).
*/
#define KMC_DIV_BY_32 0x05u

/**
* @def KMC_DIV_BY_64
* @brief Constant representing for 6bit right shift (division by 64).
*/
#define KMC_DIV_BY_64 0x06u

/**
* @def KMC_DIV_BY_128
* @brief Constant representing for 7bit right shift (division by 128).
*/
#define KMC_DIV_BY_128 0x07u

/**
* @def KMC_DIV_BY_256
* @brief Constant representing for 8bit right shift (division by 256).
*/
#define KMC_DIV_BY_256 0x08u

/**
* @def KMC_DIV_BY_256
* @brief Constant representing for 8bit right shift (division by 256).
*/
#define KMC_DIV_BY_65536 0x10u

/**
* @def KMC_MULT_BY_2
* @brief Constant representing for 1bit left shift (multiplication by 2).
*/
#define KMC_MULT_BY_2 0x01u

/**
* @def KMC_MULT_BY_4
* @brief Constant representing for 2bit left shift (multiplication by 4).
*/
#define KMC_MULT_BY_4 0x02u

/**
* @def KMC_MULT_BY_8
* @brief Constant representing for 3bit left shift (multiplication by 8).
*/
#define KMC_MULT_BY_8 0x03u

/**
* @def KMC_MULT_BY_16
* @brief Constant representing for 4bit left shift (multiplication by 16).
*/
#define KMC_MULT_BY_16 0x04u

/**
* @def KMC_MULT_BY_32
* @brief Constant representing for 5bit left shift (multiplication by 32).
*/
#define KMC_MULT_BY_32 0x05u

/**
* @def KMC_MULT_BY_64
* @brief Constant representing for 6bit left shift (multiplication by 64).
*/
#define KMC_MULT_BY_64 0x06u

/**
* @def KMC_MULT_BY_128
* @brief Constant representing for 7bit left shift (multiplication by 128).
*/
#define KMC_MULT_BY_128 0x07u

/**
* @def KMC_MULT_BY_256
* @brief Constant representing for 8bit left shift (multiplication by 256).
*/
#define KMC_MULT_BY_256 0x08u

/**
* @def KMC_CONV_MILLISECONDS_TO_SECONDS
* @brief Conversion factor to convert milliseconds to seconds (1000 milliseconds = 1 second).
*/
#define KMC_CONV_MILLISECONDS_TO_SECONCS 1000UL

/**
* @def KMC_CONV_SECONDS_TO_MILLISECONDS
* @brief Conversion factor to convert seconds to milliseconds (1 second = 1000 milliseconds).
*/
#define KMC_CONV_SECONCS_TO_MILLISECONDS 1000UL

/**
* @def KMC_CONV_MICROSECONDS_TO_SECONDS
* @brief Conversion factor to convert microseconds to seconds (1000000 microseconds = 1 second).
*/
#define KMC_CONV_MICROSECONDS_TO_SECONCS 1000000ULL

/**
* @def KMC_CONV_SECONDS_TO_MICROSECONDS
* @brief Conversion factor to convert seconds to microseconds (1 second = 1000000 microseconds).
*/
#define KMC_CONV_SECONCS_TO_MICROSECONDS 1000000ULL

/**
* @def KMC_CONV_MILLISECONDS_TO_MICROSECONDS
* @brief Conversion factor to convert milliseconds to microseconds (1000 milliseconds = 1000000 microseconds).
*/
#define KMC_CONV_MILLISECONDS_TO_MICROSECONCS 1000UL

/**
* @def KMC_CONV_MICROSECONDS_TO_MILLISECONDS
* @brief Conversion factor to convert microseconds to milliseconds (1 microsecond = 0.001 milliseconds).
*/
#define KMC_CONV_MICROSECONCS_TO_MILLISECONDS 1000UL

/**
 * @def KMC_UNUSED_8_BIT
 * @brief An 8-bit value used to represent an unused parameter.
 *
 * This constant defines an 8-bit hexadecimal value (0xFFu) that is commonly used
 * to indicate that a parameter or variable is not in use.
 */
#define KMC_UNUSED_8_BIT 0xffu

/**
 * @def KMC_UNUSED_16_BIT
 * @brief A 16-bit value used to represent an unused parameter.
 *
 * This constant defines a 16-bit hexadecimal value (0xFFFFu) that is typically
 * used to indicate that a parameter or variable is not in use.
 */
#define KMC_UNUSED_16_BIT 0xffffu

/**
 * @def KMC_UNUSED_32_BIT
 * @brief A 32-bit value used to represent an unused parameter.
 *
 * This constant defines a 32-bit hexadecimal value (0xFFFFFFFFu) that is often
 * used to signify that a parameter or variable is not in use.
 */
#define KMC_UNUSED_32_BIT 0xffffffffu

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* KM_COMMON_DEFS_H_ */
