/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage
* @section pageTOC Content
* @brief Generic common file.
* - kmCommon.h
* - kmCommonDefs.h
* - kmConfig.h
*
*  **Created on**: Aug 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmCommon library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*  Exammple usage:
@code

#include "kmCommon/kmCommon.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

int main(void) {
	while(true) {
	}
}
@endcode
*/

#ifndef KM_COMMON_H_
#define KM_COMMON_H_
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <avr/io.h>

#include "../common.h"
#include "kmConfig.h"
#include "kmCommonDefs.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */

#ifdef KM_GTEST
#include "gtest/gtest.h"
#endif /* KM_GTEST */

#endif /* KM_COMMON_H_ */
