# ReadMe
# kmCommon library for AVR MCUs

This repository contains the kmCommon library, a generic common file for AVR MCUs and also kmCommonDefs, a collection of useful macros for value manipulation and calculations. It also includes constants and conversion factors for time and bit manipulation.

## Table of Contents
- [Version History](#version-history)
- [Overview](#overview)
- [Usage](#usage)
- [Example Code](#example-code)
- [Author and License](#author-and-license)

## Version History
- v1.0.4 Fixed KMC_SIGNED_ZERO macro (2024-12-22)
- v1.0.3 Added KMC_7_BIT_SHIFT definition (2024-11-24)
- v1.0.2 Fix in Added KMC_7_OUT_OF_16_BIT_MASK_LO and KMC_9_BIT_SHIFT definition, fix in KMC_SIGNED_ZERO (2024-11-17)
- v1.0.1 Fix in KMC_VAR_NAME_VALUE macro, added KMC_ARRAY_SIZE macro, doxygen fixes (2024-08-04)
- v1.0.0 Initial (2024-05-25)

## Overview
The `kmCommon` library provides generic common files of _kmFramework_ for AVR microcontrollers.
The `kmCommonDefs` provides macros for value manipulation, such as getting the string representation of a value, finding the minimum and maximum of two values, and clamping a value within a specified range. It also includes definitions of various constants and conversion factors commonly used for bit manipulation and time conversions.


## Usage
Getting this library and adding it to own project:
- To add this module to own project as submodule - enter the main directory of the source code and use git command

``` bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmCommon.git kmCommon
```
- After cloning own application from git repository use following additional git command to get correct revision of submodule:
``` bash
git submodule update --init
```

Include “kmCommon.h” in each header and source file of the project

## Example Code
```c
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <util/delay.h>

#include "kmCommon/kmCommon.h"

int main(void) {
    // Example usage of macros
    uint8_t a = 5
    uint8_t b = 10
uint8_t c = KMC_MIN(a, b)
uint8_t d = KMC_MAX(a, b)

    while(true) {
    }
}

```

## Author and License
Author: Krzysztof Moskwa

e-mail: chris[dot]moskva[at]gmail[dot]com

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)